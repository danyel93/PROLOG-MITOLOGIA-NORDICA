%% HECHOS EN LA MITOLOGIA NORDICA INTEGRANTES GEHD,DCP
%% SON PADRES
proge(bor,vali).
proge(bor,odin).
proge(bor,ve).
proge(odin,heimdall).
proge(odin,vidar).
proge(odin,thor).
proge(odin,meili).
proge(odin,balder).
proge(odin,hoder).
proge(odin,hermod).
proge(odin,tyr).
proge(odin,bragi).
proge(odin,vali).
proge(thor,thrud).
proge(thor,ull).
proge(thor,lorride).
proge(thor,modi).
proge(thor,magni).
proge(balder,forseti).
proge(bestla,vali).
proge(bestla,odin).
proge(bestla,ve).
proge(aegir,heimdall).
proge(grior,vidar).
proge(jord,thor).
proge(jord,meili).
proge(frigg,balder).
proge(frigg,hoder).
proge(frigg,hermod).
proge(frigg,tyr).
proge(frigg,bragi).
proge(rind,vali).
proge(sif,thrud).
proge(sif,ull).
proge(sif,lorride).
proge(jarnsaxa,modi).
proge(jarnsaxa,magni).
proge(nanna,forseti).

proge(farbauti,loki).
proge(laufey,loki).
proge(farbauti,helblindi).
proge(laufey,helblindi).
proge(farbauti,byleistr).
proge(laufey,byleistr).
proge(loki,fenrir).
proge(angrbode,fenrir).
proge(loki,jormungord).
proge(angrbobe,jormungord).
proge(loki,hel).
proge(angrbode,hel).
proge(loki,nafi).
proge(sigyn,nafi).
proge(loki,vali).
proge(sigyn,vali).
proge(loki,eisa).
proge(glut,eisa).
proge(loki,elnmyria).
proge(glut,elnmyria).
%%SON ESPOSOS
%%MITOLOGIA NORDICA
esposos(bestla,bor).
esposos(bor,bestla).
esposos(odin,aegir).
esposos(aegir,odin).
esposos(odin,grior).
esposos(grior,odin).
esposos(odin,jord).
esposos(jord,odin).
esposos(odin,frigg).
esposos(frigg,odin).
esposos(odin,rind).
esposos(rind,odin).
esposos(thor,sif).
esposos(sif,thor).
esposos(thor,jarnsaxa).
esposos(jarnsaxa,thor).
esposos(balder,nanna).
esposos(nanna,balder).
esposos(bragi,idunn).
esposos(idunn,bragi).

esposos(farbauti,laufey).
esposos(laufey,farbauti).
esposos(loki,angrbode).
esposos(angrbode,loki).
esposos(loki,sigyn).
esposos(sigyn,loki).
esposos(loki,glut).
esposos(glut,loki).
%%SON HOMBRES
%%MITOLOGIA NORDICA
hombre(bor).
hombre(vali).
hombre(ve).
hombre(odin).
hombre(heimdall).
hombre(vidar).
hombre(thor).
hombre(meili).
hombre(balder).
hombre(hoder).
hombre(hermod).
hombre(tyr).
hombre(bragi).
hombre(forseti).
hombre(thrud).
hombre(lorride).
hombre(ull).
hombre(modi).
hombre(magni).

hombre(farbauti).
hombre(loki).
hombre(helblindi).
hombre(byleistr).
hombre(nafi).
hombre(fenrir).
hombre(vali).


%%SON MUJERES
%MITOLOGIA NORDICA
mujer(bestla).
mujer(aegir).
mujer(grior).
mujer(jord).
mujer(frigg).
mujer(rind).
mujer(sif).
mujer(jarnsaxa).
mujer(nanna).
mujer(idunn).

mujer(laufey).
mujer(angrbode).
mujer(sigyn).
mujer(glut).
mujer(hel).
mujer(jormungord).
mujer(eisa).
mujer(elnmyria).

nombre(dios_del_trueno_y_las_tormentas,thor).
nombre(isla_de_la_hoja,laufey).
nombre(el_que_golpea_peligrosamente,farbauti).
nombre(gigante_de_hielo,bestla).
nombre(dios_de_la_elocuencia,bragi).
nombre(diosa_de_muerte,hel).
nombre(serpiente_gigantes,jormungand).
nombre(dios_del_invierno,ull).
nombre(dios_de_la_guerra_y_el_silencio,vidar).
nombre(diosa_de_la_fertilidad,sif).
nombre(lobo_monstruoso,fenrir).
nombre(dios_del_mal_del_fuego_y_del_da�o,loki).
nombre(dios_de_la_guerra,tyr).
nombre(dios_de_la_lucha,odin).
nombre(diosa_de_la_eterna_juventud,idunn).
nombre(diosa_del_amor_la_fertilidad_y_el_matrimonio,frigg).
nombre(giganta,jarnsaxa).
nombre(dios_de_la_batalla,modi).
nombre(dios_de_la_fuerza,magni).
nombre(dios_ciego,hoder).
nombre(padre_de_los_dioses,bor).
nombre(dios_de_los_arqueros,vali).
nombre(dios_de_la_luz,balder).
nombre(mensajero_de_los_dioses,hermod).
nombre(diosa_aesir,nanna).
nombre(dios_de_la_luz,heimdall).
nombre(diosa_de_la_tierra_helada,rind).
nombre(diosa_del_silencio_y_primavera,grid).

enemigo(loki,thor).
enemigo(thor,loki).
enemigo(jormungard,thor).
enemigo(thor,jormungard).
enemigo(balder,loki).
enemigo(vidar,fenrir).
enemigo(fenrir,odin).
enemigo(odin,fenrir).
enemigo(fenrir,tyr).
enemigo(tyr,fenrir).
enemigo(heimdall,loki).
enemigo(loki,heimdall).




%Reglas
%M=madre
%P=padre
%H=hijos
%A=abuelos
%N=nietos
%H1,H2=hermanos
%CA=cu�ada
%CO=cu�ado
%S=suegros
%N=nuera
%T=tio
%D=dioses


%%PADRES
padre(P,H):-proge(P,H),hombre(P).
madre(M,H):-proge(M,H),mujer(M).
%%HIJOS
hijo(H,P):-proge(P,H),hombre(H).
hija(H,P):-proge(P,H),mujer(H).
%%ABUELOS
abuelo(P,N):-padre(P,H),proge(H,N).
abuela(M,N):-madre(M,H),proge(H,N).
%%NIETOS
nieto(N,A):-proge(A,H),proge(H,N),hombre(N).
nieta(N,A):-proge(A,H),proge(H,N),mujer(N).
%%HERMANOS
hermanos(H1,H2):-proge(P,H1),proge(P,H2).
hermano(H1,H2):-hermanos(H1,H2),hombre(H1),madre(M,H1),madre(M,H2).
%%%%%%%%%%%%%SOBRINOS
sobrino(X,Y):-hombre(X),hermanos(Y,Z),proge(Z,X).
%%%%%%%%%%%%%CU�ADOS
%cunada(CA,H1) :-mujer(CA),esposos(CA,H1),hermanos(H2,H1).
%%%%%%%%%%%%%SUEGROS
suegro(P,S):-hombre(P),esposos(S,Z),proge(P,Z).
suegra(M,S):-mujer(M),esposos(S,Z),proge(M,Z).
%%%%%%%%%%%%%NUERA
nuera(P,NR):-mujer(P),proge(NR,Z),hombre(Z),esposos(Z,P).
%%%%%%%%%%%%%TIO
%tio(P,T):-hombre(P),hermanos(P,Z),proge(Z,T).
dioses(D,N):-nombre(D,N).
enemigos(E,N):-enemigo(E,N).









